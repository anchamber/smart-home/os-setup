
## Dependencies
- python
    - openshift (on nodes)

## Roles
- jnv.unattended-upgrades (ansible-galaxy install jnv.unattended-upgrades)
- community.kubernetes (ansible-galaxy collection install community.kubernetes)
- hashicorp valut (ansible-galaxy collection install community.hashi_vault)